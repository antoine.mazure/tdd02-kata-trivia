package com.adaptionsoft.games.uglytrivia;

public class Player {
    public String name;
    public Place place;
    public int purse;
    public boolean inPenaltyBox;
    public boolean isGettingOutOfPenaltyBox;

    public Player(String name, Place firstPlace) {
        this.name = name;
        this.place = firstPlace;
        this.purse = 0;
        this.inPenaltyBox = false;
    }

    @Override
    public String toString() {
        return name;
    }
}
