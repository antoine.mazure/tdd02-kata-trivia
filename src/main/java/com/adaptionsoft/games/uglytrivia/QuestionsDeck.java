package com.adaptionsoft.games.uglytrivia;

import java.util.LinkedList;

public class QuestionsDeck {
    QuestionCategory category;
    LinkedList<Question> questions;

    public QuestionsDeck(QuestionCategory category) {
        this.category = category;

        this.questions = new LinkedList<>();
        for (int i = 0; i < 50; i++) {
            questions.add(new Question(category + " Question " + i));
        }
    }

    @Override
    public String toString() {
        return category.toString();
    }
}
