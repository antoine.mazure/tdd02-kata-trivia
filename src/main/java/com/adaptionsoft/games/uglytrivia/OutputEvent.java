package com.adaptionsoft.games.uglytrivia;

public interface OutputEvent {

    public void outputEvent(Object event);

}
