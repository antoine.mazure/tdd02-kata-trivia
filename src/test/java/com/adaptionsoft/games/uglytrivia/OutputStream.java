package com.adaptionsoft.games.uglytrivia;

public class OutputStream implements OutputEvent{
    public String allLogs = "";

    @Override
    public void outputEvent(Object event) {
        allLogs += event.toString();
    }
}
