package com.adaptionsoft.games.uglytrivia;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class GameTest {

    int fakeRandom = 0;

    @Test
    public void goldenMaster1() {
        ArrayList players = new ArrayList();
        players.add("Chet");
        players.add("Pat");
        players.add("Sue");

        boolean notAWinner;

        OutputStream outputStream = new OutputStream();
        Game aGame = new Game(outputStream);

        aGame.addPlayer("Chet");
        aGame.addPlayer("Pat");
        aGame.addPlayer("Sue");

        do {
            aGame.roll(5);
            if (false) {
                notAWinner = aGame.wrongAnswer();
            } else {
                notAWinner = aGame.wasCorrectlyAnswered();
            }
        } while (notAWinner);

        assertEquals("Chet was addedThey are player number 1Pat was addedThey are player number 2Sue was addedThey are player number 3Chet is the current playerThey have rolled a 5Chet's new location is 5The category is ScienceScience Question 0Answer was corrent!!!!Chet now has 1 Gold Coins.Pat is the current playerThey have rolled a 5Pat's new location is 5The category is ScienceScience Question 1Answer was corrent!!!!Pat now has 1 Gold Coins.Sue is the current playerThey have rolled a 5Sue's new location is 5The category is ScienceScience Question 2Answer was corrent!!!!Sue now has 1 Gold Coins.Chet is the current playerThey have rolled a 5Chet's new location is 10The category is SportsSports Question 0Answer was corrent!!!!Chet now has 2 Gold Coins.Pat is the current playerThey have rolled a 5Pat's new location is 10The category is SportsSports Question 1Answer was corrent!!!!Pat now has 2 Gold Coins.Sue is the current playerThey have rolled a 5Sue's new location is 10The category is SportsSports Question 2Answer was corrent!!!!Sue now has 2 Gold Coins.Chet is the current playerThey have rolled a 5Chet's new location is 3The category is RockRock Question 0Answer was corrent!!!!Chet now has 3 Gold Coins.Pat is the current playerThey have rolled a 5Pat's new location is 3The category is RockRock Question 1Answer was corrent!!!!Pat now has 3 Gold Coins.Sue is the current playerThey have rolled a 5Sue's new location is 3The category is RockRock Question 2Answer was corrent!!!!Sue now has 3 Gold Coins.Chet is the current playerThey have rolled a 5Chet's new location is 8The category is PopPop Question 0Answer was corrent!!!!Chet now has 4 Gold Coins.Pat is the current playerThey have rolled a 5Pat's new location is 8The category is PopPop Question 1Answer was corrent!!!!Pat now has 4 Gold Coins.Sue is the current playerThey have rolled a 5Sue's new location is 8The category is PopPop Question 2Answer was corrent!!!!Sue now has 4 Gold Coins.Chet is the current playerThey have rolled a 5Chet's new location is 1The category is ScienceScience Question 3Answer was corrent!!!!Chet now has 5 Gold Coins.Pat is the current playerThey have rolled a 5Pat's new location is 1The category is ScienceScience Question 4Answer was corrent!!!!Pat now has 5 Gold Coins.Sue is the current playerThey have rolled a 5Sue's new location is 1The category is ScienceScience Question 5Answer was corrent!!!!Sue now has 5 Gold Coins.Chet is the current playerThey have rolled a 5Chet's new location is 6The category is SportsSports Question 3Answer was corrent!!!!Chet now has 6 Gold Coins.", outputStream.allLogs);
    }


    @Test
    public void goldenMaster2() {
        ArrayList players = new ArrayList();
        players.add("Chet");
        players.add("Pat");
        players.add("Sue");

        boolean notAWinner;

        OutputStream outputStream = new OutputStream();
        Game aGame = new Game(outputStream);

        aGame.addPlayer("Chet");
        aGame.addPlayer("Pat");
        aGame.addPlayer("Sue");

        do {
            aGame.roll(5);
            if (returnTrueEvery5Call()) {
                notAWinner = aGame.wrongAnswer();
            } else {
                notAWinner = aGame.wasCorrectlyAnswered();
            }
        } while (notAWinner);

        assertEquals("Chet was addedThey are player number 1Pat was addedThey are player number 2Sue was addedThey are player number 3Chet is the current playerThey have rolled a 5Chet's new location is 5The category is ScienceScience Question 0Answer was corrent!!!!Chet now has 1 Gold Coins.Pat is the current playerThey have rolled a 5Pat's new location is 5The category is ScienceScience Question 1Answer was corrent!!!!Pat now has 1 Gold Coins.Sue is the current playerThey have rolled a 5Sue's new location is 5The category is ScienceScience Question 2Answer was corrent!!!!Sue now has 1 Gold Coins.Chet is the current playerThey have rolled a 5Chet's new location is 10The category is SportsSports Question 0Answer was corrent!!!!Chet now has 2 Gold Coins.Pat is the current playerThey have rolled a 5Pat's new location is 10The category is SportsSports Question 1Answer was corrent!!!!Pat now has 2 Gold Coins.Sue is the current playerThey have rolled a 5Sue's new location is 10The category is SportsSports Question 2Question was incorrectly answeredSue was sent to the penalty boxChet is the current playerThey have rolled a 5Chet's new location is 3The category is RockRock Question 0Answer was corrent!!!!Chet now has 3 Gold Coins.Pat is the current playerThey have rolled a 5Pat's new location is 3The category is RockRock Question 1Answer was corrent!!!!Pat now has 3 Gold Coins.Sue is the current playerThey have rolled a 5Sue is getting out of the penalty boxSue's new location is 3The category is RockRock Question 2Answer was correct!!!!Sue now has 2 Gold Coins.Chet is the current playerThey have rolled a 5Chet's new location is 8The category is PopPop Question 0Answer was corrent!!!!Chet now has 4 Gold Coins.Pat is the current playerThey have rolled a 5Pat's new location is 8The category is PopPop Question 1Answer was corrent!!!!Pat now has 4 Gold Coins.Sue is the current playerThey have rolled a 5Sue is getting out of the penalty boxSue's new location is 8The category is PopPop Question 2Question was incorrectly answeredSue was sent to the penalty boxChet is the current playerThey have rolled a 5Chet's new location is 1The category is ScienceScience Question 3Answer was corrent!!!!Chet now has 5 Gold Coins.Pat is the current playerThey have rolled a 5Pat's new location is 1The category is ScienceScience Question 4Answer was corrent!!!!Pat now has 5 Gold Coins.Sue is the current playerThey have rolled a 5Sue is getting out of the penalty boxSue's new location is 1The category is ScienceScience Question 5Answer was correct!!!!Sue now has 3 Gold Coins.Chet is the current playerThey have rolled a 5Chet's new location is 6The category is SportsSports Question 3Answer was corrent!!!!Chet now has 6 Gold Coins.", outputStream.allLogs);
    }

    @Test
    public void goldenMaster3() {
        ArrayList players = new ArrayList();
        players.add("Chet");
        players.add("Pat");
        players.add("Sue");

        boolean notAWinner;

        OutputStream outputStream = new OutputStream();
        Game aGame = new Game(outputStream);

        aGame.addPlayer("Chet");
        aGame.addPlayer("Pat");
        aGame.addPlayer("Sue");

        do {
            aGame.roll(4);
            if (returnTrueEvery5Call()) {
                notAWinner = aGame.wrongAnswer();
            } else {
                notAWinner = aGame.wasCorrectlyAnswered();
            }
        } while (notAWinner);

        assertEquals("Chet was addedThey are player number 1Pat was addedThey are player number 2Sue was addedThey are player number 3Chet is the current playerThey have rolled a 4Chet's new location is 4The category is PopPop Question 0Answer was corrent!!!!Chet now has 1 Gold Coins.Pat is the current playerThey have rolled a 4Pat's new location is 4The category is PopPop Question 1Answer was corrent!!!!Pat now has 1 Gold Coins.Sue is the current playerThey have rolled a 4Sue's new location is 4The category is PopPop Question 2Answer was corrent!!!!Sue now has 1 Gold Coins.Chet is the current playerThey have rolled a 4Chet's new location is 8The category is PopPop Question 3Answer was corrent!!!!Chet now has 2 Gold Coins.Pat is the current playerThey have rolled a 4Pat's new location is 8The category is PopPop Question 4Answer was corrent!!!!Pat now has 2 Gold Coins.Sue is the current playerThey have rolled a 4Sue's new location is 8The category is PopPop Question 5Question was incorrectly answeredSue was sent to the penalty boxChet is the current playerThey have rolled a 4Chet's new location is 0The category is PopPop Question 6Answer was corrent!!!!Chet now has 3 Gold Coins.Pat is the current playerThey have rolled a 4Pat's new location is 0The category is PopPop Question 7Answer was corrent!!!!Pat now has 3 Gold Coins.Sue is the current playerThey have rolled a 4Sue is not getting out of the penalty boxChet is the current playerThey have rolled a 4Chet's new location is 4The category is PopPop Question 8Answer was corrent!!!!Chet now has 4 Gold Coins.Pat is the current playerThey have rolled a 4Pat's new location is 4The category is PopPop Question 9Answer was corrent!!!!Pat now has 4 Gold Coins.Sue is the current playerThey have rolled a 4Sue is not getting out of the penalty boxQuestion was incorrectly answeredSue was sent to the penalty boxChet is the current playerThey have rolled a 4Chet's new location is 8The category is PopPop Question 10Answer was corrent!!!!Chet now has 5 Gold Coins.Pat is the current playerThey have rolled a 4Pat's new location is 8The category is PopPop Question 11Answer was corrent!!!!Pat now has 5 Gold Coins.Sue is the current playerThey have rolled a 4Sue is not getting out of the penalty boxChet is the current playerThey have rolled a 4Chet's new location is 0The category is PopPop Question 12Answer was corrent!!!!Chet now has 6 Gold Coins.", outputStream.allLogs);
    }

    private boolean returnTrueEvery5Call() {
        if (fakeRandom >= 5) {
            fakeRandom = 0;
            return true;
        }
        fakeRandom += 1;
        return false;
    }
}